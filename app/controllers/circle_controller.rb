require 'net/http'

class CircleController < ActionController::Base
  def circle_hook
    create_circle_status
    send_circle_status
  end

  private

  def send_circle_status
    uri = URI('https://c9c3e0a8.ngrok.io/circle_status')
    params = { status: circle_status, project: project_name, user: user_name }
    uri.query = URI.encode_www_form(params)
    Net::HTTP.get_response(uri)
  end

  def create_circle_status
    CircleStatus.create!(status: circle_status, project: project_name, user: user_name)
  end

  def project_name
    @project_name ||= params[:payload][:compare].split('/')[4]
  end

  def circle_status
    @circle_status ||= params[:payload][:status] == 'failed' ? 0 : 1
  end

  def user_name
    @user_name ||= params[:payload][:commiter_name]
  end
end
