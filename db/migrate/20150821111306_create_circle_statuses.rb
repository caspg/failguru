class CreateCircleStatuses < ActiveRecord::Migration
  def change
    create_table :circle_statuses do |t|
      t.integer :status
      t.string :project

      t.timestamps null: false
    end
  end
end
