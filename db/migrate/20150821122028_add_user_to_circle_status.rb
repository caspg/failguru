class AddUserToCircleStatus < ActiveRecord::Migration
  def change
    add_column :circle_statuses, :user, :string
  end
end
